---
author: Mireille Coilhac
title: IDE hors d'une admonition
tags:
  - 2-fonction
---

La fonction `addition` prend en paramètres deux nombres entiers ou flottants, et renvoie la somme des deux.

Compléter : 

{{ IDE('scripts/addition') }}
